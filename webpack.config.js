const path = require('path');

module.exports = {
    mode: 'development',
    entry: [
        './src/app.ts'
    ],
    module: {
        rules: [
            {
                test: /\.ts$/,
                include: [path.resolve(__dirname, 'src')],
                use: 'ts-loader',
            },
            {
                test: /\.(scss|css)$/,
                include: [path.resolve(__dirname, 'src')],
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    devServer: {
        proxy: {
            '/apps': {
              target: 'https://textprof.dockserver.de/',
              changeOrigin: true,
            },
        },
    },
    devtool: 'inline-source-map',
    output: {
        publicPath: 'auto',
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public'),
    },
};