export interface Step {

    storeData(): void;
    loadData(): void;

    execute(): void;
}