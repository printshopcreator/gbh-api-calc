import {State} from "./state";
import {Step1} from "./step1";
import {Step2} from "./step2";
import {Step3} from "./step3";
import {Step4} from "./step4";
import {Step5} from "./step5";
import {Step6} from "./step6";
import {Step} from "./stepInterface";

export class Wizard {
    private state: State;

    private step: Step;

    constructor(state: State) {
        this.state = state;
        this.step = new Step1(this.state);
        this.bindNextButtons();
    }

    private process(step: number) {
        if(this.state.actStep != 0) {
            this.step.storeData();
        }
        this.state.actStep = step;
        this.toogleTabPanes();

        if(this.state.actStep == 2) {
            this.step = new Step2(this.state);
        }

        if(this.state.actStep == 3) {
            this.step = new Step3(this.state);
        }

        if(this.state.actStep == 4) {
            this.step = new Step4(this.state);
        }

        if(this.state.actStep == 5) {
            this.step = new Step5(this.state);
        }

        if(this.state.actStep == 6) {
            this.step = new Step6(this.state);
        }
        this.step.loadData();
        this.step.execute();
    }

    private toogleTabPanes() {

        document.querySelectorAll('.tab-pane').forEach(elm => {
            elm.classList.remove('show');
            elm.classList.remove('active');
        });

        document.querySelectorAll('.nav .nav-link').forEach(elm => {
            elm.classList.remove('active');
        });

        document.querySelector('#step' + this.state.actStep)?.classList.add('active');
        document.querySelector('#step' + this.state.actStep)?.classList.add('show');
        document.querySelector('#step' + this.state.actStep + '-tab')?.classList.add('active');
    }

    private bindNextButtons() {
        document.querySelectorAll('.btnToStep1').forEach(elm => {
            elm.addEventListener('click', (e) => {
                this.process(1);
            });
        });
        document.querySelectorAll('.btnToStep2').forEach(elm => {
            elm.addEventListener('click', (e) => {
                this.process(2);
            }); 
        });
        document.querySelectorAll('.btnToStep3').forEach(elm => {
            elm.addEventListener('click', (e) => {
                this.process(3);
            });   
        });
        document.querySelectorAll('.btnToStep4').forEach(elm => {
            elm.addEventListener('click', (e) => {
                this.process(4);
            });   
        });
        document.querySelectorAll('.btnToStep5').forEach(elm => {
            elm.addEventListener('click', (e) => {
                this.process(5);
            }); 
        });
        document.querySelectorAll('.btnToStep6').forEach(elm => {
            elm.addEventListener('click', (e) => {
                let checkedAgb = document.querySelector('#stamm_checkDaten') as HTMLInputElement;
                if(checkedAgb.checked) {
                    this.process(6);
                }else{
                    let checkAgbTxt = document.querySelector('.checkAgb');
                    checkAgbTxt?.classList.add('bg', 'bg-danger');
                } 
            });   
        });
        document.querySelectorAll('#send_offer').forEach(elm => {
            elm.addEventListener('click', (e) => {
                let checkedAgb = document.querySelector('#stamm_checkDaten') as HTMLInputElement;
                if(checkedAgb.checked) {
                    this.state.sendOffer();
                }else{
                    let checkAgbTxt = document.querySelector('.checkAgb');
                    checkAgbTxt?.classList.add('bg', 'bg-danger');
                }
            });
        });
        this.process(1);
    }
}