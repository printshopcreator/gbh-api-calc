import {State} from "./state";
import {Step} from "./stepInterface";

export class Step6 implements Step{
    private state: State;

    constructor(state: State) {
        this.state = state;
        this.bindNextButtons();
    }

    private bindNextButtons() {

    }

    public execute() {

    }

    storeData(): void {
    }

    loadData(): void {

        let txtAddress =
            this.state.stamm.company  + "<br/>" +
            this.state.stamm.salutation + " " + this.state.stamm.firstname + " " + this.state.stamm.lastname + "<br/>" +
            this.state.stamm.zip + " " + this.state.stamm.city + "<br/>" +
            this.state.stamm.country;

        let txtAddressElm = document.querySelector('#txt_address');
        if(txtAddressElm) {
            txtAddressElm.innerHTML = txtAddress;
        }

        let txtNameElm = document.querySelector('#txt_name');
        if(txtNameElm) {
            txtNameElm.innerHTML = this.state.stamm.salutation + " " + this.state.stamm.firstname + " " + this.state.stamm.lastname;
        }

        let txtDateElm = document.querySelector('#txt_date');
        if(txtDateElm) {
            let today = new Date();
            txtDateElm.innerHTML = today.toLocaleString();
        }

        let txtPriceElm = document.querySelector('#txt_price');
        if(txtPriceElm) {
            txtPriceElm.innerHTML = this.state.price;
        }
    }
}