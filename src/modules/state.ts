export class State {
    get price(): string {
        return this._price;
    }

    set price(value: string) {
        this._price = value;
    }
    private _actStep: number = 0;

    private _calcValues:any = {}

    private _bs: string = "";
    private _vs: string = "";
    private _vo: any = {};
    private _stamm: any = {};

    private _price:string = "";

    private _productUUID: string;

    constructor(productUUID: string) {
        this._productUUID = productUUID;
        this.loadLocal();
    }

    get actStep(): number {
        return this._actStep;
    }
    set actStep(value: number) {
        this._actStep = value;
    }
    get productUUID(): string {
        return this._productUUID;
    }
    get calcValues(): any {
        return this._calcValues;
    }
    set calcValues(value: any) {
        this._calcValues = value;
        localStorage.setItem('calcValues', JSON.stringify(this.calcValues));
    }
    get bs(): string {
        return this._bs;
    }
    set bs(value: string) {
        this._bs = value;
        localStorage.setItem('bs', JSON.stringify(this.bs));
    }
    get vs(): string {
        return this._vs;
    }
    set vs(value: string) {
        this._vs = value;
        localStorage.setItem('vs', JSON.stringify(this.vs));
    }
    get vo(): any {
        return this._vo;
    }
    set vo(value: any) {
        this._vo = value;
        localStorage.setItem('vo', JSON.stringify(this.vo));
    }
    get stamm(): any {
        return this._stamm;
    }
    set stamm(value: any) {
        this._stamm = value;
        localStorage.setItem('stamm', JSON.stringify(this.stamm));
    }

    private loadLocal() {
        let calcValues = localStorage.getItem('calcValues');
        if(calcValues != null) {
            this.calcValues = JSON.parse(calcValues);
        }
        let bs = localStorage.getItem('bs');
        if(bs != null) {
            this.bs = JSON.parse(bs);
        }
        let vs = localStorage.getItem('vs');
        if(vs != null) {
            this.vs = JSON.parse(vs);
        }
        let vo = localStorage.getItem('vo');
        if(vo != null) {
            this.vo = JSON.parse(vo);
        }
        let stamm = localStorage.getItem('stamm');
        if(stamm != null) {
            this.stamm = JSON.parse(stamm);
        }


    }

    sendOffer(): void {
        let obj = {
            stamm: this.stamm,
            calcValues: this.calcValues,
            vo: this.vo,
            vs: this.vs,
            bs: this.bs,
            productUUID: this.productUUID
        }

        fetch('/apps/finish/api', {
            method: 'POST',
            headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(obj)
        })
            .then(response => response.json())
            .then(data => {
                if(data.success) {
                    document.querySelector('#show_send')?.classList.add('show');
                }
            }).catch(error => {
            // Handle error
        });
    }
}