import {State} from "./state";
import {Step} from "./stepInterface";

export class Step3 implements Step{
    private state: State;

    constructor(state: State) {
        this.state = state;
        this.bindNextButtons();
    }

    private bindNextButtons() {

    }

    public execute() {

    }

    public storeData(): void {
        let vs = document.querySelector('#vs_comment') as HTMLTextAreaElement;
        if(vs) {
            this.state.vs = vs.value;
        }
    }

    loadData(): void {
        let vs = document.querySelector('#vs_comment') as HTMLTextAreaElement;
        if(vs) {
            vs.value = this.state.vs;
        }
    }
}