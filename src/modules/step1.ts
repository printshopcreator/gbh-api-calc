import {State} from "./state";
import {Step} from "./stepInterface";

export class Step1 implements Step {
    private state: State;
    private showPrice: boolean = false;
    private formArray:any = {};

    constructor(state: State) {
        this.bindCalcForm();
        this.state = state;
    }

    execute() {


        let elements = document.querySelectorAll('#CALCFORM input') as any as Array<HTMLInputElement>;

        elements.forEach((e: HTMLInputElement) => {
            this.formArray[e.id] = e.value;
        });

        fetch('/apps/api/plugin/system/psc/xmlcalc/price', {
            method: 'POST',
            headers: {
                'content-type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify({product: this.state.productUUID, test: false, values : this.formArray})
        })
        .then(response => response.json())
        .then(data => {
            this.buildForm(data.elements['hydra:member']);
            if(this.showPrice) {
                this.buildPrice(data);
            }else{
                this.formArray = this.state.calcValues;
                let elements = document.querySelectorAll('#CALCFORM input') as any as Array<HTMLInputElement>;
                elements.forEach((e: HTMLInputElement) => {
                    if(this.formArray[e.id]) {
                        e.value = this.formArray[e.id];
                    }
                });
            }
        }).catch(error => {
            // Handle error
        });
    }

    private buildForm(elements: any) {
        elements.forEach((element: any) => {
            this.buildElement(element);
        });

        let submitButton = document.createElement('input');
        submitButton.type = 'submit';
        submitButton.textContent = 'Calc';
        submitButton.classList.add('btn');
        submitButton.classList.add('btn-primary');
  
        let containerSubmit = document.createElement('div');
        containerSubmit.id = 'container_submit';
        containerSubmit.appendChild(submitButton);

        this.addOrReplaceFormElement("submit", containerSubmit, true);
    }

    private calc() {
        this.showPrice = true;
        this.execute();
    }

    private buildElement(element: any) {
        let label = document.createElement('label');
        label.classList.add('form-label');
        label.textContent = element.name;

        let obj: any = document.createElement('input');
        if(element.htmlType == "hidden") {
            obj.type = 'hidden';
            obj.id = element.id;
            obj.name = element.id;
            obj.value = element.rawValue;
            
            this.addOrReplaceFormElement(element.id, obj, element.valid);
            return;
        }
        if(element.htmlType == "select") {
            obj = document.createElement('select');
            obj.type = 'text';
            obj.id = element.id;
            obj.name = element.id;
            obj.value = element.rawValue;
            obj.classList.add('form-control');

            element.options.forEach((opt: any) => {
                if(opt.valid) {
                    obj.append(new Option(opt.name, opt.id, opt.selected, opt.selected));
                }
            });
        }  
        if(element.htmlType == "input") {
            obj = document.createElement('input');
            obj.type = 'text';
            obj.id = element.id;
            obj.name = element.id;
            obj.value = element.rawValue;
            obj.required = true;
            obj.classList.add('form-control');
        }
        if(element.htmlType == "checkbox") {
            obj = document.createElement('input');
            obj.type = 'checkbox';
            obj.id = element.id;
            obj.name = element.id;
            obj.value = element.rawValue;
            obj.classList.add('form-control');
        }
        if(element.htmlType == "textarea") {
            obj = document.createElement('textarea');
            obj.type = 'checkbox';
            obj.id = element.id;
            obj.name = element.id;
            obj.value = element.rawValue;
            obj.classList.add('form-control');
        }

        let container = document.createElement('div');
        container.id = 'container_' + element.id;
        container.appendChild(label);
        container.appendChild(obj);
        
        this.addOrReplaceFormElement(element.id, container, element.valid);
  
    }

    private addOrReplaceFormElement(id: string, element: HTMLElement, valid: boolean) {
        
        if(document.querySelector('#container_' + id) != null) {
            if(!valid) {
                document.querySelector('#container_' + id)?.remove();
            }else{
                document.querySelector('#container_' + id)?.replaceWith(element);
            }
        }else{
            document.querySelector('#CALCFORM')?.appendChild(element);  
        }
    }

    private buildPrice(data: any) {
        let netto = document.querySelector('#price_netto');
        let steuer = document.querySelector('#price_steuer');
        let brutto = document.querySelector('#price_brutto');
        if(brutto) {
            brutto.textContent = (data.brutto/100).toLocaleString('de-DE', {
                style: 'currency',
                currency: 'EUR',
            });
            this.state.price = (data.brutto/100).toLocaleString('de-DE', {
                style: 'currency',
                currency: 'EUR',
            });
        }
        if(netto) {
            netto.textContent = (data.netto/100).toLocaleString('de-DE', {
                style: 'currency',
                currency: 'EUR',
            });
        }
        if(steuer) {
            steuer.textContent = (data.steuer/100).toLocaleString('de-DE', {
                style: 'currency',
                currency: 'EUR',
            });
        }

        let calcResultFrame = document.querySelector('.calc_result');
        if(calcResultFrame) {
            calcResultFrame.classList.add('show');
        }
    }

    private bindCalcForm() {
        let calcForm = document.querySelector('#CALCFORM');
        if(calcForm) {
            calcForm.addEventListener('submit', (e) => {
                e.preventDefault();
                this.calc();
                return false;
            });
        }
    }

    storeData(): void {
        this.state.calcValues = this.formArray;
    }

    loadData(): void {

    }
}