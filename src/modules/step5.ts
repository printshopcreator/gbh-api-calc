import {State} from "./state";
import {Step} from "./stepInterface";

export class Step5 implements Step{
    private state: State;
    private formArray:any = {};

    constructor(state: State) {
        this.state = state;
        this.bindNextButtons();
    }

    private bindNextButtons() {

    }

    public execute() {

    }

    storeData(): void {
        let elements = document.querySelectorAll('#STAMMFORM input, #STAMMFORM select') as any as Array<HTMLInputElement>;

        elements.forEach((e: HTMLInputElement) => {
            this.formArray[e.id] = e.value;
        });

        this.state.stamm = this.formArray;
    }

    loadData(): void {
        this.formArray = this.state.stamm;
        let elements = document.querySelectorAll('#STAMMFORM input, #STAMMFORM select') as any as Array<HTMLInputElement>;
        elements.forEach((e: HTMLInputElement) => {
            if(this.formArray[e.id]) {
                e.value = this.formArray[e.id];
            }
        });
    }
}