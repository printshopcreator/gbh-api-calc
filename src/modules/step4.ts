import {State} from "./state";
import {Step} from "./stepInterface";

export class Step4 implements Step{
    private state: State;
    private formArray:any = {};

    constructor(state: State) {
        this.state = state;
        this.bindNextButtons();
    }

    private bindNextButtons() {

    }

    public execute() {

    }

    storeData(): void {
        let elements = document.querySelectorAll('#VOFORM input') as any as Array<HTMLInputElement>;

        elements.forEach((e: HTMLInputElement) => {
            this.formArray[e.id] = e.value;
        });

        this.state.vo = this.formArray;
    }

    loadData(): void {
        this.formArray = this.state.vo;
        let elements = document.querySelectorAll('#VOFORM input') as any as Array<HTMLInputElement>;
        elements.forEach((e: HTMLInputElement) => {
            if(this.formArray[e.id]) {
                e.value = this.formArray[e.id];
            }
        });
    }
}