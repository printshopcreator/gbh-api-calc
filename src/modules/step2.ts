import {State} from "./state";
import {Step} from "./stepInterface";

export class Step2 implements Step{
    private state: State;

    constructor(state: State) {
        this.state = state;
        this.bindNextButtons();
    }

    private bindNextButtons() {

    }

    public execute() {

    }

    public storeData(): void {
        let bs = document.querySelector('#bs_comment') as HTMLTextAreaElement;
        if(bs) {
            this.state.bs = bs.value;
        }
    }

    loadData(): void {
        let bs = document.querySelector('#bs_comment') as HTMLTextAreaElement;
        if(bs) {
            bs.value = this.state.bs;
        }
    }
}