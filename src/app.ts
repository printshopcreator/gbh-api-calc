import './style.scss';
import {State} from "./modules/state";
import { Step1 } from './modules/step1';
import {Wizard} from "./modules/wizard";

var ready = (callback: any) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => {

    let productUUID = "0001-00010102-5784d853-51a2-2811e11e";

    let state = new State(productUUID);
    let wizard = new Wizard(state);

})
